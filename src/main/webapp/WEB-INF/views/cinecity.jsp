<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="/stylesheet/main.css"/>
</head>

<body>

<%
    String movieTitle = request.getParameter("movieTitle");
    if (movieTitle == null) {
    	movieTitle = "Lagoa Azul";
    }
    pageContext.setAttribute("movieTitle", movieTitle);
    UserService userService = UserServiceFactory.getUserService();
    User user = userService.getCurrentUser();
    if (user != null) {
        pageContext.setAttribute("user", user);
%>

<p>Hello, ${fn:escapeXml(user.nickname)}! (You can
    <a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">sign out</a>.)</p>
<%
} else {
%>
<p>Hello!
    <a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Sign in</a>
    to include your name with greetings you post.</p>
<%
    }
%>


<form action="/cinecity.jsp" method="get">
    <div><input type="text" name="movieTitle" value="${fn:escapeXml(movieTitle)}"/></div>
    <div><input type="submit" value="View"/></div>
</form>

</body>
</html>